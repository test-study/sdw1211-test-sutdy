var assert = require('assert'), should = require("should");
var memory1 = 8, memory2 = 4, memory3 = 3, memory4 = 8;
//var computer8gigaMemory = 8, computer4gigaMemory = 4, mobile3GigaMemory = 3, mobile8gigaMemory = 8;
var System = function() {
    var memorySize = 0;
    
    return {
        addMemory : function(memory) {
            memorySize += memory;
        },
        getMemorySize : function() {
            return memorySize;
        }
    };
};


describe('cohesion', function() {
    it('check computer memory size', function() {
        var computer = new System;
        //var computer8gigaMemory = 8, computer4gigaMemory = 4
        
        computer.addMemory(memory1);
        computer.addMemory(memory2);
        
        //computer.addMemory(computer8gigaMemory);
        //computer.addMemory(computer4gigaMemory);
        should(computer.getMemorySize()).be.exactly(12);
    });
    
    it('check mobile memory size', function() {
        var mobile = new System;
        //mobile3GigaMemory = 3, mobile8gigaMemory = 8
        
        mobile.addMemory(memory3);
        mobile.addMemory(memory4);

        //computer.addMemory(mobile3GigaMemory);
        //computer.addMemory(mobile8gigaMemory);
        
        should(mobile.getMemorySize()).be.exactly(11);
    });
});


