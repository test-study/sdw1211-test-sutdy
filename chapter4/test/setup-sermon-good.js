var should = require('should');


var FakeDB = function() {
	return {
		datas : [],
		clear : function(action) {
			var err = false;

			this.datas = [];
			action(err);
		}, save : function(users, action) {
			
			this.datas = this.datas.concat(users);
			action();
		}, find : function(type, action) {
			if (type.type !== "User") { action(false, null); return; }
			
			action(false, this.datas);
		}
	};
};


describe('Connection', function() {
	var db = new FakeDB,
		tobi = { name :"tobi"},
		loki = { name :"loki"},
		jane = { name :"jane"};

	beforeEach(function(done) {
		db.clear(function(err) {
			if(err) return done(err);

			db.save([tobi, loki, jane], done);
		});
	});

	describe('#find()', function() {
		it('user search', function(done) {
			db.find({type:"User"}, function(err, res) {
				if (err) {
					done(err); 
					return;
				}
				
				should(res).have.property('length', 3);
				done();
			});
		});
	});
});

