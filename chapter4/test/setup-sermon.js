var should = require('should');


describe('setup-sermon', function() {
	var db;
	beforeEach(function() {
	    
	   var  tobi = { name :"tobi"},
		    loki = { name :"loki"},
		    jane = { name :"jane"};
		    
        db = (function() {
            	return {
            		datas : [],
            		clear : function(action) {
            			var err = false;
            
            			this.datas = [];
            			action();
            		}, save : function(users) {
            			
            			this.datas = this.datas.concat(users);
            			
            		}, find : function(type, action) {
            			if (type.type !== "User") { action(false, null); return; }
            			
            			action(false, this.datas);
            		}
            	};
            })(); 

		db.clear(function() {
			
			db.save([tobi, loki, jane]);
		});
	});

	describe('#find()', function() {
		it('user search', function() {
			db.find({type:"User"}, function(err, res) {
				if (err) {
					err.should.throw();
				
				}
				
				should(res).have.property('length', 3);
				
			});
		});
	});
});

