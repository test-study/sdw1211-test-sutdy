var assert = require('assert'), should = require("should");

var UpDown = function() {
    var value = 0;
    
    value = parseInt(Math.random() * 10);

    return {
        getValue : function() {
          return value;   
        },
        try : function(v) {
            if (value > v) {
                return "UP";
            } else if (value < v) {
                return "DOWN";
            } else {
                return "ANSWER!!";
            }
        }
    }
}

describe('primitive-assertion', function() {
	describe('bad example', function() {
		it('Updown game test', function() {
            var updown = new UpDown(), testValue = 4;
            
            assert.ok(updown.getValue() > 0, "양수 체크");
   
            if (updown.getValue() === testValue) {
                assert.ok("ANSWER!!" === updown.try(testValue), "같은 경우 ANSWER!!");
            } else if (updown.getValue() > testValue) {
                assert.ok("UP" === updown.try(testValue), "클 경우 UP");
            } else if (updown.getValue() < testValue) {
                assert.ok("DOWN" === updown.try(testValue), "작을 경우 DOWN");
            }
		});
	});
	
	describe('good example', function() {
		it('Updown game test', function(done) {
            var updown = new UpDown(), testValue = 4;
            
            updown.getValue().should.be.above(0);
    
            if (updown.getValue() === testValue) {
                updown.try(testValue).should.be.exactly("ANSWER!!");
            } else if (updown.getValue() > testValue) {
                updown.try(testValue).should.be.exactly("UP");
            } else if (updown.getValue() < testValue) {
                updown.try(testValue).should.be.exactly("DOWN");
            }        
		});
	});
	
});