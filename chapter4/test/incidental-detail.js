var assert = require('assert'), should = require("should");

var UpDown = function() {
    var value = 0;
    
    value = parseInt(Math.random() * 10);

    return {
        getValue : function() {
          return value;   
        },
        try : function(v) {
            if (value > v) {
                return "UP";
            } else if (value < v) {
                return "DOWN";
            } else {
                return "ANSWER!!";
            }
        }
    }
}

describe('icidental-detail', function() {
	describe('bad example', function() {
		it('Updown game test', function() {
            var updown = new UpDown(), testValue = 4;
            
            updown.getValue().should.be.above(-1);
    
            if (updown.getValue() === testValue) {
                updown.try(testValue).should.be.exactly("ANSWER!!");
            } else if (updown.getValue() > testValue) {
                updown.try(testValue).should.be.exactly("UP");
            } else if (updown.getValue() < testValue) {
                updown.try(testValue).should.be.exactly("DOWN");
            }
            
          
		});
	});
	
	describe('good example', function() {
		it('Updown game test', function() {
            var updown = new UpDown(), testValue = 4;
            
            updown.getValue().should.be.above(-1);
            assertTryValueExactly(updown, testValue);
		});
		
		function assertTryValueExactly(updown, testValue) {
            if (updown.getValue() === testValue) {
                updown.try(testValue).should.be.exactly("ANSWER!!");
            } else if (updown.getValue() > testValue) {
                updown.try(testValue).should.be.exactly("UP");
            } else if (updown.getValue() < testValue) {
                updown.try(testValue).should.be.exactly("DOWN");
            }
		}
	});
	
});