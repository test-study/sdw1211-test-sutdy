var assert = require('assert'), employees = [
	{name : "david", isSupervisor : true, isNew : false},
	{name : "tom", isSupervisor : false, isNew : false},
	{name : "oki", isSupervisor : false, isNew : false},
	{name : "ada", isSupervisor : false, isNew : false},
	{name : "minho", isSupervisor : false, isNew : true},
	{name : "manho", isSupervisor : false, isNew : false},
	{name : "manja", isSupervisor : false, isNew : false}
];


describe('conditional logic', function() {
	describe('bad example', function() {
		it('search new or supervisor', function() {
		    var i = employees.length;
		    
		    while(i--) {
		        if (employees[i].isSupervisor) {
		            employees[i].name.should.be.exactly("david");
		        }
		        if (employees[i].isNew) {
		            employees[i].name.should.be.exactly("minho");
		        }
		    }
		});
	});
	
	describe('good example', function() {
		it('search new or supervisor', function() {

		    checkNewOrSupervisor(employees, "isSupervisor", "david");
		    checkNewOrSupervisor(employees, "isNew", "minho");
		});
		
		function checkNewOrSupervisor(employees, type, name) {
		    var i = employees.length;
		    
		    while(i--) {
		        if (employees[i][type]) {
		            employees[i].name.should.be.exactly(name);
		            return;
		        }
		    }
		    
		    should.fail();
		}
	});
    
});