var assert = require('assert'), should = require("should");
var memory1 = 8, memory2 = 4, memory3 = 3, memory4 = 8;
//var computer8gigaMemory = 8, computer4gigaMemory = 4, mobile3GigaMemory = 3, mobile8gigaMemory = 8;
var System = function() {
    var memorySize = 0;
    
    return {
        addMemory : function(memory) {
            
            if (memorySize > 20) {
                
                throw new Error("Max size");
            }
            memorySize += memory;
        },
        getMemorySize : function() {
            return memorySize;
        }
    };
};

describe('cohesion', function() {
    it('check computer memory size', function() {
        var computer = new System;
        
        //메모리 8기가 추가
        computer.addMemory(memory1);
        //메모리 4기가 추가
        computer.addMemory(memory2);
        
        //12기가가 맞는지 확인
        should(computer.getMemorySize()).be.exactly(12);
    });
    

    it('check computer memory size', function() {
        var computer = new System, memory4giga = 4, memory8giga = 8, memory12giga = 12;
        
        
        computer.addMemory(memory4giga);
        computer.addMemory(memory8giga);
        
        should(computer.getMemorySize()).be.exactly(memory12giga);
    });
    
    it('Never failing tests', function(done) {
           var computer = new System, memory4giga = 4, memory8giga = 8, memory12giga = 13;
           
            // try {
            //     computer.addMemory(memory4giga);
            //     computer.addMemory(memory8giga);
            //     computer.addMemory(memory12giga);
            //     //computer.addMemory(memory4giga);
            //     //should.fail("20giga check fail");
            // } catch (e) {
            //     should(e.message).be.containEql("Max size");
            // }
            
            should(computer.addMemory(memory4giga)).not.throw();
            should(computer.addMemory(memory8giga)).not.throw();
            should(computer.addMemory(memory12giga)).not.throw();
            should(computer.addMemory(memory4giga)).throw();    
            done();
    });

});


