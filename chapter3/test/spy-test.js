var testArray = [];

Array.prototype.checkPush = function(v) {
    var i = this.length;
    while(i--) {
        if (this[i] === v) return true;
    }
    
    return false;
};

describe('Array', function() {
	describe('#push()', function() {
		it('check push data', function(done) {
			
			testArray.push(10);	
			testArray.push(11);	
			testArray.push(12);	
			testArray.push(13);	
			
			var result = testArray.checkPush(10);
			
			if (result) done();
			else done(false);
		});
	});
});