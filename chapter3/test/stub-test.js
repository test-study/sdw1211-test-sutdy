var should = require('should');

function MailStub() {
    return {
        send : function() { return true; }
    };
}

var mail = new MailStub;

describe('Mail', function() {
	describe('#send()', function() {
		it('user search', function(done) {
			var result = mail.send();

			if (result) done();
			else done(false);
		});
	});
});