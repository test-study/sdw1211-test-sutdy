var assert = require('assert'), employees = [
	{name : "david", isSupervisor : true, isNew : false},
	{name : "tom", isSupervisor : false, isNew : false},
	{name : "oki", isSupervisor : false, isNew : false},
	{name : "ada", isSupervisor : false, isNew : false},
	{name : "minho", isSupervisor : false, isNew : true},
	{name : "manho", isSupervisor : false, isNew : true},
	{name : "manja", isSupervisor : false, isNew : true}
];

describe('duplication', function() {
	describe('bad example', function() {
		it('check supervisor count', function() {
			var count = 0;
            for (var i=0; i < employees.length; i++) {
            	if (employees[i].isSupervisor) {
            		count++;	
            	}
            }
            
            count.should.be.exactly(1);
		});
		it('check new employees count', function() {
			var i = employees.length, count = 0;
			
			while(i--) {
            	if (employees[i].isNew) {
            		count++;	
            	}
			}
			
            count.should.be.exactly(3);
		});
	});
	
	describe('good example', function() {
		it('check supervisor count', function() {
			var count = getCountWhatEmployees(function(employee) {
				return employee.isSupervisor;
			});

            count.should.be.exactly(1);
		});
		it('check new employees count', function() {
			var count = getCountWhatEmployees(function(employee) {
				return employee.isNew;
			});
            count.should.be.exactly(3);
		});
		
		function getCountWhatEmployees(what) {
			var i = employees.length, count = 0;
			
			while(i--) {
            	if (what(employees[i])) {
            		count++;	
            	}
			}
			
			return count;
		}
	});
	
});