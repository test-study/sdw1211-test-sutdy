var assert = require('assert'), message = {
    "success" : "success",
    "failure" : "failure",
    "apple" : "apple"
};

describe('duplication', function() {
	describe('bad example', function() {
		it('success test', function() {
            message["success"].should.be.exactly("success");
		});
		it('failure test', function() {
            message["failure"].should.be.exactly("failure");
		});
	});
	
	describe('good example', function() {
		it('success test', function() {
		    var key = "success";
            message[key].should.be.exactly(key);
            //checkExactlyKeyAndMessage("success");
		});
		it('failure test', function() {
		    var key = "failure";
            message[key].should.be.exactly(key);
            //checkExactlyKeyAndMessage("failure");
		});
		
		function checkExactlyKeyAndMessage(key) {
		    message[key].should.be.exactly(key);
		}
	});
	
});